import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as signalR from '@microsoft/signalr';
import { BehaviorSubject, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SignalrService {
  private connection !: signalR.HubConnection;
  private urlSignalR = 'https://www.dlztecnologia.com/Hubs';
  private methodName = 'ReceiveMessage';
  private data$: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(private http: HttpClient) { }

  get data() {
    return this.data$;
  }

  completeSubjects() {
    if(this.data$) this.data$.complete();
  }

  createConnection(): Promise<any> {
    return new Promise((res, rej) => {
      this.connection  = new signalR.HubConnectionBuilder()
        .withUrl(this.urlSignalR)
        .build();
      this.connection.start()
        .then(() => { console.log('Connection started'); res(true); })
        .catch(err => { console.log('Error while starting connection: ', err); rej(err) })
    })
  }

  register() {
    this.connection.on(this.methodName, (data) => this.data$.next(data));
  }
}
