import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  private url = 'http://api.openweathermap.org/data/2.5/weather';

  constructor(private http: HttpClient) { }

  get weather() {
    const params = new HttpParams().set('id', '3448439'); // São Paulo
    return this.http.get<any>(this.url, { params }).pipe(
      map(res => {
        if(res?.main?.temp) {
          res.main.temp = Math.floor(res.main.temp - 273.15);
        }
        return res;
      })
    );
  }
}
