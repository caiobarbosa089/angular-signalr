import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class WeatherInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if (!environment.weatherKey) return next.handle(request);
    // Url key words
    const needKey = ['openweathermap'];
    // RegExp form every key word
    const rgs = needKey.map((elem) => new RegExp(`^(?=.*\\b${elem}).*$`, 'gmi'));
    // Check if the URL has the key word
    const canAuthorize = !!rgs.find((elem) => request.url.match(elem));
    // Create request clone with appid
    const authReq = request.clone({
      params: request.params.set('appid', `${environment.weatherKey}`),
    });
    // Return with appid if find key word
    return canAuthorize ? next.handle(authReq) : next.handle(request);
  }
}
