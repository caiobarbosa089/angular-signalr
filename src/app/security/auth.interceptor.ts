import { Injectable, Optional } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { from, Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { mergeMap, take } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(@Optional() private authService: AuthService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return from(this.authService.getToken()).pipe(
      take(1),
      mergeMap((token) => {
        if (!token) return next.handle(request);
        // Url key words
        const needAuthorization = ['localhost'];
        // RegExp form every key word
        const rgs = needAuthorization.map((elem) => new RegExp(`^(?=.*\\b${elem}).*$`, 'gmi'));
        // Check if the URL has the key word
        const canAuthorize = !!rgs.find((elem) => request.url.match(elem));
        // Create request clone with authentication
        const authReq = request.clone({
          headers: request.headers.set('Authorization', `Bearer ${token}`),
        });
        // Return with authorization if find key word
        return canAuthorize ? next.handle(authReq) : next.handle(request);
      }),
    );
  }
}
