import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private authorized: boolean = false;

  async getToken(): Promise<string> {
    return 'my-token';
  }

  async isAuthorized(): Promise<boolean> {
    return this.authorized;
  }

  toggleAuth() {
    this.authorized = !this.authorized;
  }
}
