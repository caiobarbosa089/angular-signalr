import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanActivateChild,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { from, Observable, of } from 'rxjs';
import { catchError, tap, timeoutWith } from 'rxjs/operators';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivateChild, CanActivate {

  constructor(private route: Router, private auth: AuthService, private snackBar: MatSnackBar) {}

  get police() {
    return from(this.auth.isAuthorized()).pipe(
      timeoutWith(500, of(false)),
      catchError(() => of(false)),
      tap(async (can) => {
        if (can) return;
        this.snackBar.open('Unauthorized', 'ok', {
          horizontalPosition: 'right',
          duration: 5000
        })
        this.route.navigate(['/']);
      }),
    )
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.police;
  }

  canActivateChild(
    childRoute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.police;
  }
}
