import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './security/auth.guard';
import { BodyComponent } from './views/body/body.component';
import { VideoSenderComponent } from './views/video-sender/video-sender.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '', component: BodyComponent, children: [
    { path: 'home', component: VideoSenderComponent },
    { 
      path: 'signalr',
      canActivate: [AuthGuard],
      loadChildren: () => import('./views/signal-r/signal-r.module').then((mod) => mod.SignalRModule)
    }
  ] },
  { path: '**', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
