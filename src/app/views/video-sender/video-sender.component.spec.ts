import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoSenderComponent } from './video-sender.component';

describe('VideoSenderComponent', () => {
  let component: VideoSenderComponent;
  let fixture: ComponentFixture<VideoSenderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VideoSenderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoSenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
