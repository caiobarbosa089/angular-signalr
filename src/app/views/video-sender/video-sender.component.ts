import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/security/auth.service';
import { WeatherService } from 'src/app/service/weather.service';

@Component({
  selector: 'app-video-sender',
  templateUrl: './video-sender.component.html',
  styleUrls: ['./video-sender.component.scss']
})
export class VideoSenderComponent implements OnInit {
  weather;
  isAuth = false;

  constructor(private weatherService: WeatherService, private authService: AuthService) { }

  async ngOnInit(): Promise<void> {
    this.weatherService.weather.subscribe(val => this.weather = val);
    this.isAuth = await this.authService.isAuthorized();
  }

  toggleAuth() {
    this.authService.toggleAuth();
    this.isAuth = !this.isAuth;
  }
}
