import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-card-specs',
  templateUrl: './card-specs.component.html',
  styleUrls: ['./card-specs.component.scss']
})
export class CardSpecsComponent {
  @Input() spec: any;
}
