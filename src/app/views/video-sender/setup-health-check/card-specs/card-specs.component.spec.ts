import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardSpecsComponent } from './card-specs.component';

describe('CardSpecsComponent', () => {
  let component: CardSpecsComponent;
  let fixture: ComponentFixture<CardSpecsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardSpecsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardSpecsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
