import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetupHealthCheckComponent } from './setup-health-check.component';

describe('SetupHealthCheckComponent', () => {
  let component: SetupHealthCheckComponent;
  let fixture: ComponentFixture<SetupHealthCheckComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetupHealthCheckComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetupHealthCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
