import { Component, OnInit } from '@angular/core';
import * as Bowser from "bowser";
import { SpeedTestService } from 'ng-speed-test';

interface IData {
  title: string,
  value: any
}

@Component({
  selector: 'app-setup-health-check',
  templateUrl: './setup-health-check.component.html',
  styleUrls: ['./setup-health-check.component.scss']
})
export class SetupHealthCheckComponent implements OnInit {
  specs: IData[] = [];
  analyzing = true;

  constructor(private speedTestService: SpeedTestService) { }

  async ngOnInit(): Promise<void> {
    this.specs = await Promise.all([
      this.browserCheck(),
      this.osCheck(),
      this.cameraCheck(),
      this.micCheck(),
      this.broadbandCheck()
    ]);
    this.analyzing = false;
  }

  async browserCheck(): Promise<IData> {
    const bowser = Bowser.getParser(window.navigator.userAgent);
    const browser = bowser.getBrowser();
    return { title: 'Browser', value: `${browser.name} ${browser.version}` };
  }

  async osCheck(): Promise<IData> {
    const bowser = Bowser.getParser(window.navigator.userAgent);
    const os = bowser.getOS();
    return { title: 'OS', value: `${os.name} ${os.versionName}` };
  }

  async cameraCheck() {
    try {
      if (navigator.mediaDevices || navigator.mediaDevices.enumerateDevices) {
        const medias = await navigator.mediaDevices.enumerateDevices();
        return { title: 'Camera', value: !!medias.find(media => media.kind === 'videoinput') };
      }  
      return { title: 'Camera', value: false };
    } catch (error) {
      return { title: 'Camera', value: false };
    }
  }

  async micCheck(): Promise<IData> {
    try {
      if (navigator.mediaDevices || navigator.mediaDevices.enumerateDevices) {
        const medias = await navigator.mediaDevices.enumerateDevices();
        return { title: 'Mic', value: !!medias.find(media => media.kind === 'audioinput') };
      }  
      return { title: 'Mic', value: false };
    } catch (error) {
      return { title: 'Mic', value: false };
    }
  }

  async broadbandCheck(): Promise<IData> {    
    const speed = await this.speedTestService.getMbps({ iterations: 10 }).toPromise();
    return { title: 'Broadband', value: `${Math.floor(speed)} Mbps` };
  }
}
