import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignalRComponent } from './signal-r.component';

const routes: Routes = [{ path: '', component: SignalRComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SignalRRoutingModule { }
