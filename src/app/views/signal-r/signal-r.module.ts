import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignalRRoutingModule } from './signal-r-routing.module';
import { SignalRComponent } from './signal-r.component';
import { MaterialModule } from 'src/app/material.module';
import { SignalrService } from 'src/app/service/signalr.service';

@NgModule({
  imports: [
    CommonModule,
    SignalRRoutingModule,
    MaterialModule
  ],
  declarations: [SignalRComponent],
  providers: [SignalrService]
})
export class SignalRModule { }
