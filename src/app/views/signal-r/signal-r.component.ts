import { Component, OnDestroy, OnInit } from '@angular/core';
import { SignalrService } from '../../service/signalr.service';

@Component({
  selector: 'app-signal-r',
  templateUrl: './signal-r.component.html',
  styleUrls: ['./signal-r.component.scss']
})
export class SignalRComponent implements OnInit, OnDestroy {
  data = [];  

  constructor(private signalRService: SignalrService) {}

  async ngOnInit(): Promise<void> {
    try {
      await this.signalRService.createConnection();
      this.signalRService.register();
      this.getData();
    } catch (error) { }
  }

  ngOnDestroy(): void {
    this.signalRService.completeSubjects();	
  }

  getData(): void {
    this.signalRService.data.subscribe(val => {
      console.log(val)
      this.data.push(val)
    });
  }
}
