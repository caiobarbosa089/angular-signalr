import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BodyComponent } from './views/body/body.component';
import { AuthInterceptor } from './security/auth.interceptor';
import { AuthService } from './security/auth.service';
import { VideoSenderComponent } from './views/video-sender/video-sender.component';
import { SetupHealthCheckComponent } from './views/video-sender/setup-health-check/setup-health-check.component';
import { VideoStreamingComponent } from './views/video-sender/video-streaming/video-streaming.component';
import { WebcamModule } from 'ngx-webcam';
import { SpeedTestModule } from 'ng-speed-test';
import { MaterialModule } from './material.module';
import { CardSpecsComponent } from './views/video-sender/setup-health-check/card-specs/card-specs.component';
import { WeatherService } from './service/weather.service';
import { WeatherInterceptor } from './interceptor/weather.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    BodyComponent,
    VideoSenderComponent,
    SetupHealthCheckComponent,
    VideoStreamingComponent,
    CardSpecsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule,
    WebcamModule,
    SpeedTestModule,
  ],
  providers: [
    AuthService,
    WeatherService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: WeatherInterceptor, multi: true },
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
